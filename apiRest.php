<?php
	//API REST -------------------------------------------------------------------------------------------
	//S'ha usat SLIM per tal de construir la API REST
	use \Psr\Http\Message\ServerRequestInterface as Request;
	use \Psr\Http\Message\ResponseInterface as Response;
	
	require 'Slim/vendor/autoload.php';
	require 'Base de dades/initDB.php';
	$app = new \Slim\App;
	
	
	//POST METHODS ---------------------------------------------------------------------------
	
	//Afegeix un nou artista a la BD
	$app->post('/rest/artist/register', function (Request $request, Response $response) {
			//Sobtenen els parametres i formularis del request
			$name = $request->getParam('name');
			$description = $request->getParam('description');
			try {
				//Connexio amb la BD
				$db = new dataBase('Base de dades');
				//Es busca un nou ID que no estigui assignat
				$newID=rand(1,1000);
				$newIDFinded=FALSE;
				while(!$newIDFinded){
					$sql = "SELECT code FROM artist WHERE code=$newID";
					$aResult = array();
					$results=$db->query($sql);
					while ($aRow = $results->fetchArray(SQLITE3_ASSOC)) $aResult[] = $aRow;
					if(sizeof($aResult)<1) $newIDFinded=TRUE;
					else $newID=rand(1,1000);
				}
				$db->close();
				$db = new dataBase('Base de dades');
				//Un cop trobat un ID no assignat s'afegeix el nou artista
				$sql='INSERT INTO artist (code,name,description) VALUES 
								('.$newID.',"'.$name.'","'.str_replace("\"","\"\"",$description).'");';
				$results=$db->query($sql);
				$db->close();
				//Retorna el nou objecte creat amb el nou codi
				$newElementToRet = array('code' => intval($newID), 'name' => $name, 'description' => $description);
				return json_encode(array($newElementToRet));
			} catch (Exception $e) {
					echo "ERROR";
					echo 'Caught exception: ' . $e->getMessage();
			}
	});
	
	//Afegeix un nou LP a la BD a l'artista identificat amb {id}
	$app->post('/rest/artist/{id}/LP/register', function (Request $request, Response $response) {
			//Sobtenen els parametres i formularis del request
			$artistId = $request->getAttribute('id');
			$name = $request->getParam('name');
			$description = $request->getParam('description');
			try {
				//Connexio amb la BD
				$db = new dataBase('Base de dades');
				//Es busca un nou ID que no estigui assignat
				$newID=rand(1,1000);
				$newIDFinded=FALSE;
				while(!$newIDFinded){
					$sql = "SELECT code FROM LP WHERE code=$newID";
					$aResult = array();
					$results=$db->query($sql);
					while ($aRow = $results->fetchArray(SQLITE3_ASSOC)) $aResult[] = $aRow;
					if(sizeof($aResult)<1) $newIDFinded=TRUE;
					else $newID=rand(1,1000);
				}
				//Un cop trobat un ID no assignat s'afegeix el nou LP
				$sql='INSERT INTO LP (code,name,description,fk_artist) VALUES
					('.$newID.',"'.$name.'","'.str_replace("\"","\"\"",$description).'",'.$artistId.');';
				$results=$db->query($sql);
				$db->close();
				//Retorna el nou objecte creat amb el nou codi
				$newElementToRet = array('code' => intval($newID), 'name' => $name, 'description' => $description, 'fk_artist' => intval($artistId));
				return json_encode(array($newElementToRet));
			} catch (Exception $e) {
					echo "ERROR";
					echo 'Caught exception: ' . $e->getMessage();
			}
	});
	
	//Obte fins a "limit" artistes que el seu nom comença per "seqChar"
	$app->post('/rest/artist/filter', function (Request $request, Response $response) {
			$subname = $request->getParam('seqChar');
			$limit = $request->getParam('limit');
			$db = new dataBase('Base de dades');
			if(!$db) echo $db->lastErrorMsg();
			$sql = "SELECT * FROM artist 
				WHERE name LIKE '".$subname."%' 
				LIMIT ".$limit.";";
			$results=$db->query($sql);
			$aResult = array();
			while ($aRow = $results->fetchArray(SQLITE3_ASSOC)) $aResult[] = $aRow;
			$db->close();
			return json_encode($aResult);
	});
	
	//Obte fins a "limit" LP que el seu nom comença per "seqChar"
	$app->post('/rest/LP/filter', function (Request $request, Response $response) {
			$subname = $request->getParam('seqChar');
			$limit = $request->getParam('limit');
			$db = new dataBase('Base de dades');
			if(!$db) echo $db->lastErrorMsg();
			$sql = "SELECT * FROM LP 
				WHERE name LIKE '".$subname."%' 
				LIMIT ".$limit.";";
			$results=$db->query($sql);
			$aResult = array();
			while ($aRow = $results->fetchArray(SQLITE3_ASSOC)) $aResult[] = $aRow;
			$db->close();
			return json_encode($aResult);
	});
	
	//GET METHODS ---------------------------------------------------------------------------
	
	//Obte l'artista identificat amb {id}
	$app->get('/rest/artist/{id}', function (Request $request, Response $response) {
			$id = $request->getAttribute('id');
			$db = new dataBase('Base de dades');
			if(!$db) echo $db->lastErrorMsg();
			$sql = "SELECT code,name FROM artist WHERE code=$id";
			$results=$db->query($sql);
			$aResult = array();
			while ($aRow = $results->fetchArray(SQLITE3_ASSOC)) $aResult[] = $aRow;
			$db->close();
			return json_encode($aResult);
	});
	
	//Obte tots els LP de l'artista identificat amb {id}
	$app->get('/rest/artist/{id}/LP', function (Request $request, Response $response) {
			$id = $request->getAttribute('id');
			$db = new dataBase('Base de dades');
			if(!$db) echo $db->lastErrorMsg();
			$sql = "SELECT * 
				FROM artist,LP 
				WHERE artist.code=LP.fk_artist AND artist.code=$id";
			$results=$db->query($sql);
			$aResult = array();
			while ($aRow = $results->fetchArray(SQLITE3_ASSOC)) $aResult[] = $aRow;
			$db->close();
			return json_encode($aResult);
	});
	
	//UPDATE METHODS ---------------------------------------------------------------------------
	
	//Modifica els camps de l'artista identificat amb {id}
	$app->put('/rest/artist/{id}', function (Request $request, Response $response) {
			$id = $request->getAttribute('id');
			$name = $request->getParam('name');
			$description = $request->getParam('description');
			$db = new dataBase('Base de dades');
			if(!$db) echo $db->lastErrorMsg();
			$sql = 'UPDATE artist 
				SET name="'.$name.'", description="'.str_replace("\"","\"\"",$description).'"
				WHERE code='.$id.';';
			$results=$db->query($sql);
			$db->close();
			$newElementToRet = array('code' => intval($id), 'name' => $name, 'description' => $description);
			return json_encode(array($newElementToRet));
	});
	
	//Modifica els camps de l'LP identificat amb {id}
	$app->put('/rest/LP/{id}', function (Request $request, Response $response) {
			$id = $request->getAttribute('id');
			$name = $request->getParam('name');
			$description = $request->getParam('description');
			$artistId = $request->getParam('artistId');
			$db = new dataBase('Base de dades');
			if(!$db) echo $db->lastErrorMsg();
			$sql = 'UPDATE LP 
				SET name="'.$name.'", description="'.str_replace("\"","\"\"",$description).'"
				WHERE code='.$id.';';
			$results=$db->query($sql);
			$db->close();
			$newElementToRet = array('code' => intval($id), 'name' => $name, 'description' => $description, 'fk_artist' => $artistId);
			return json_encode(array($newElementToRet));
	});
	
	//DELETE METHODS ---------------------------------------------------------------------------
	//Elimina l'artista amb code={id} i totes les seves LP asociades
	$app->delete('/rest/artist/{id}', function (Request $request, Response $response) {
			$id = $request->getAttribute('id');
			$db = new dataBase('Base de dades');
			if(!$db) echo $db->lastErrorMsg();
			$sql = 'DELETE FROM artist 
				WHERE code='.$id.';';
			$results=$db->query($sql);
			$sql = 'DELETE FROM LP 
				WHERE fk_artist='.$id.';';
			$results=$db->query($sql);
			$db->close();
			$newElementToRet = array('deleted' => true);
			return json_encode(array($newElementToRet));
	});
	
	//Elimina l'LP amb code={id}
	$app->delete('/rest/LP/{id}', function (Request $request, Response $response) {
			$id = $request->getAttribute('id');
			$db = new dataBase('Base de dades');
			if(!$db) echo $db->lastErrorMsg();
			$sql = 'DELETE FROM LP 
				WHERE code='.$id.';';
			$results=$db->query($sql);
			$db->close();
			$newElementToRet = array('deleted' => true);
			return json_encode(array($newElementToRet));
	});
	
	$app->run();
?>