Per fer la WEB i backend he usat:
HTML, Javascript, AngularJS, PHP, Slim (per fer l'API REST) i Sqlite3

L'estructura és la seguent:
	- El direcori Base de dades conte:
		- create_sqliteDB.php (crea l'estructura de la BD i l'inicialitza)
		- initDB.php (classe que extend Sqlite3, s'usa la classe per establir connecions amb la BD)
		- sqliteDB.sqlite (base de dades inicialitzada)
	- El directori CSS conte el fitxer .css
	- Images conte una imatge de fons per la web
	- llibreries (hi ha les llibreries que he usat o tenia previst usar)
	- A la carpeta Slim  hi ha la configuracio necessaria per usar el framework
	- angular.php	(Controlador ANGULAR de totes les vistes de la pagina i qui s'intercomunica amb la BD)
	- apiRest.php (API REST)
	-	index.php (Pagina inicial HTML)
	
Com a servidor he usat XAMPP.
-Tot escolta en el port 80
-Per simplificar, no s'ha modificat cap fitxer de configuracio d'apache i l'api rest es crida a traves de fitxer (no hi ha ruta)

Per instalar només cal baixar el projecte en el directori public d'apache i si no s'usa xampp i la IP localhost 
caldra canviar la direccions de les crides a la BD (caniviar variable "rutaIn") del fitxer angular.