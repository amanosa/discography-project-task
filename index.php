<!DOCTYPE HTML>
<html ng-app="angular">
	
	<head>
		<title>Discography project Task</title>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js"></script>
		<script src="llibreries/jquery.min.js"></script>
		<script src="llibreries/jquery-ui.min.js"></script>
		<script type="text/javascript" src="angular.js"></script>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"></script>
		<link rel="stylesheet" href="css/index.css" />
	</head>
	
	<body class="landing" ng-controller="mainController">
		<!--Taula que divideix en una part en la qual escollir entre la vista d'artistes o LP mes l'opcio d'afegir artistes
			i LP, i l'altre amb a vista seleccionada -->
		<table id="taulaEstructura">
			<tr>
				<!--SELECTORS DE VISTA (LP O ARTIST), I CREADOR D'ARTISTES I LP-->
				<td>
					<button type="button"  ng-class="butonARColor" ng-click="buttonChange(true);">SEARCH ARTISTS</button>
					<button type="button" ng-class="butonLPColor" ng-click="buttonChange(false);">SEARCH LPs</button>
					<hr></hr>
					<button type="button"  class="selectMenuButons" 
							ng-click="contVar.addNewArtist=!contVar.addNewArtist; contVar.addNewLP=false;">ADD NEW ARTISTS</button>
					<button type="button" class="selectMenuButons" 
							ng-click="contVar.addNewLP=!contVar.addNewLP; contVar.addNewArtist=false;">ADD NEW LP</button>
					
					<!--Creador artista-->
					<div ng-show="contVar.addNewArtist" class="afegidor">
						<label for="name">Artist name : </label>
						<input id="name" ng-model="formCreateObject.artistname" type="text" class="textIn">
						<label for="description">Description : </label>
						<input id="description" ng-model="formCreateObject.description" type="text" class="textIn">
						<br>
						<input type="submit" value="ADD" ng-click="insertToDB(true)">
					</div>	
					
					<!--Creador LP-->
					<div ng-show="contVar.addNewLP" class="afegidor">
						<label for="artist" >Artist name : </label>
						<input id="artist" type="text" class="textIn" ng-model="formCreateObject.artistname">
						<label for="name">LP Name : </label>
						<input id="name" type="text" class="textIn" ng-model="formCreateObject.lpName">
						<label for="description">Description : </label>
						<input id="description" type="text" class="textIn" ng-model="formCreateObject.description">
						<br>
						<input type="submit" value="ADD" ng-click="insertToDB(false)">
					</div>	
				</td>
				<!--VISTES (LP O ARTIST)-->
				<td>
					<h2>{{titleText}}<h2>
					<!--Filtra els noms d'aritstes o LPs-->
					<div id="llistadorCercador">
							<span>
								<input id="cercador" ng-model="mysearch" ng-keyup="opcioSelectAutoComplete(mysearch,contVar.showArtists,false)">
								<button type="button" class="botoCercador" ng-class="butonLPColor">
									<i class="fa fa-search"></i>
								</button>
							</span>
						<div id="llistador">
							<ul>
								<li ng-repeat="artist in artists;" 
										ng-hide="(!contVar.showArtists) || (mysearch && (artist.name).indexOf(mysearch) < 0);"
										ng-click="selectArtistToShow(artist); getArtistLPS(artist); contVar.artistLPList=false; elSel.LPSelected=''">
									{{artist.name}}
								</li>
								<li ng-repeat="LP in LPs" 
										ng-hide="(contVar.showArtists) || (mysearch && (LP.name).indexOf(mysearch) < 0)"
										ng-click="selectLPToShow(LP)">
									{{LP.name}}
								</li>
							</ul>
						</div>
					</div>
					<!--Mostra l'LP o artista seleccionat detalladament-->
					<div id="mostrador">
							<!--Mostra l'artista seleccionat detalladament, ames del NUMERO DE LP i un enllaç a una vista on hi ha
							totes les LP de l'artista. Permet editar o eliminar l'artista.-->
							<div ng-show="contVar.showArtists">
								<!--Informacio artista-->
								<div ng-show="!contVar.artistLPList && elSel.artistSelected">
									<p>ARTIST BASIC INFORMATION:</p>
									<p>NAME:</p> 
									<textarea rows="1" ng-model="nameArtAct"></textarea>
									<p>DESCRIPTION:</p> 
									<textarea rows="6" ng-model="descArtAct"></textarea>
									<input type="submit" value="UPDATE DATA" ng-click="setArtist(elSel.artistSelected)">
									<input type="submit" value="DELETE ARTIST" ng-click="deleteArtist(elSel.artistSelected);">
									<p>THIS ARTIST HAS &nbsp <b>{{getNumberArtistLPS(elSel.artistSelected)}}</b> &nbsp LPs, LOOK FOR HIS <a href="" ng-click="contVar.artistLPList=true;"> LPs LIST </a></p>
								</div>	
								<!--Llista LP i LP manager-->
								<div ng-show="contVar.artistLPList">
									<button style="float:right; margin-right:5px;" ng-click="contVar.artistLPList=false; elSel.LPSelected=''">
										BACK TO {{elSel.artistSelected.name}} BASIC INFO
									</button>
									<p>ARTIST {{elSel.artistSelected.name}} LPS:</p>
									<ul class="lpList">
										<li ng-repeat="LP in LPs" ng-show="contVar.showArtists && LP.fk_artist==elSel.artistSelected.code"
												ng-click="selectLPToShow(LP)">
											{{LP.name}}
										</li>	
									</ul>
									<div ng-show="elSel.LPSelected">
										<p>LP INFORMATION</p>
										<p>LP NAME:</p> 
										<textarea rows="1" ng-model="nameLPAct"> </textarea>
										<p>DESCRIPTION:</p> 
										<textarea rows="6" ng-model="descLPAct"> </textarea>
										<input type="submit" value="UPDATE DATA" ng-click="setLP(elSel.LPSelected)">
										<input type="submit" value="DELETE ARTIST" ng-click="deleteLP(elSel.LPSelected)">
									</div>
								</div>
							</div>
							<!--Mostra l'LP seleccionat detalladament i permet editar o esborrar l'LP-->
							<div ng-show="!contVar.showArtists && elSel.LPSelected">
								<p>LP BASIC INFORMATION:</p>
								<p>NAME:</p> 
								<textarea rows="1" ng-model="nameLPAct"></textarea>
								<p>DESCRIPTION:</p> 
								<textarea rows="6" ng-model="descLPAct"> </textarea>
								<input type="submit" value="UPDATE DATA" ng-click="setLP(elSel.LPSelected)">
								<input type="submit" value="DELETE LP" ng-click="deleteLP(elSel.LPSelected)">
							</div>	
					</div>
					<div>
						
					</div>
				</td>
			</tr>
			<img src="Images/background Images.png" width="100" height="140">
		</table>
		
	</body>
</html>