//Controlador de la pagina, gestiona la vista i fa les peticions pertinents a la BD
var app=angular.module('angular', []);  
app.controller("mainController", function ($scope, $filter, $http, $timeout) {
  
	//RUTA INICIAL BD
	var rutaIn = "http://localhost/";
	
	//ATRIBUTS - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	$scope.artists=[];
	$scope.LPs=[];
	$scope.titleText = "ARTIST PAGE";
	$scope.butonARColor="selectMenuButonClik";
	$scope.butonLPColor="selectMenuButonUnclik";
	$scope.mysearch="";
	$scope.elSel={
		artistSelected:"",
		LPSelected:""
	}
	$scope.contVar={
			addNewArtist: false,
			addNewLP: false,
			showArtists: true,
			artistLPList: false
	}
	$scope.formCreateObject={
			artistname: "",
			lpName: "",
			description: ""
	}
	
	
	//FUNCIONS CONTROL DE VISTA - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	
	//Pre: - -
	//Post: Si "artistsButton" s'ha canviat la vista de LPs a artistes i viseversa
	$scope.buttonChange = function(artistsButton){
		$scope.contVar.showArtists=artistsButton; 
		if(artistsButton){
			$scope.titleText="ARTIST PAGE";
			$scope.butonARColor="selectMenuButonClik";
			$scope.butonLPColor="selectMenuButonUnclik";
		}
		else{
			$scope.titleText="LP'S PAGE";
			$scope.butonLPColor="selectMenuButonClik";
			$scope.butonARColor="selectMenuButonUnclik";
		}
		$scope.mysearch=""; 
	}
	
	//Pre: - -
	//Post: S'ha seleccionat l'artista "artist" per mostrar detalladament
	$scope.selectArtistToShow=function(artist){
		$scope.elSel.artistSelected=artist;
		$scope.nameArtAct=artist.name;
		$scope.descArtAct=artist.description;
	}
	
	//Pre: - -
	//Post: S'ha seleccionat el LP "lp" per mostrar detalladament
	$scope.selectLPToShow=function(lp){
		$scope.elSel.LPSelected=lp;
		$scope.nameLPAct=lp.name;
		$scope.descLPAct=lp.description;
	}
	
	
	//FUNCIONS CONTROL BD - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	
	//Pre: - -
	//Post: Si "isArtist" s'ha afegit un nou artista a la BD, altrament un LP
	$scope.insertToDB=function(isArtist){
		if(isArtist){
			var parametres = {
				name:  $scope.formCreateObject.artistname,
				description: $scope.formCreateObject.description
			};	
			$http.post(rutaIn+'apiRest.php/rest/artist/register',parametres)
				.success(function (data, status) {
					for (var clau in data) {
						if (data.hasOwnProperty(clau) && $scope.filtrarArtista(data[clau].code).length==0){ 
							$scope.artists.push(data[clau]);
						}
					}
				})
				.error(function (data, status) {
					alert("Error adding artist");
				});
		}
		else{
			var parametres = {
				name:  $scope.formCreateObject.lpName,
				description: $scope.formCreateObject.description
			};	
			var found = $filter('filter')($scope.artists, {name: $scope.formCreateObject.artistname}, true);
			var id=found[0].code;
			$http.post(rutaIn+'apiRest.php/rest/artist/'+id+'/LP/register',parametres)
				.success(function (data, status) {
					for (var clau in data) {
						if (data.hasOwnProperty(clau) && $scope.filtrarLPs(data[clau].code).length==0){ 
							$scope.LPs.push(data[clau]);
						}
					}
				})
				.error(function (data, status) {
					alert("Error adding artist LP");
				});
		}
		$scope.contVar.addNewArtist=false;
		$scope.contVar.addNewLP=false;
	}
	
	//Pre: 	- - 
	//Post:	S'han obtingut del servidor els 10 primers artistes o LP que comencen per  "nomsel". 
	//			Si "artist" busca artistes, altrament LP's
	var tempsespera;
	$scope.opcioSelectAutoComplete=function(nomsel,artist,initCall){
	    if (tempsespera) $timeout.cancel(tempsespera);
      tempsespera = $timeout(function() {
				if((nomsel && /^\w+$/.test(nomsel)) || initCall){
					var parametres = {
						seqChar:  nomsel,
						limit: 10
					};	
					if(initCall) parametres.limit=6;
					if(artist){
						$http.post(rutaIn+'apiRest.php/rest/artist/filter',parametres)
							.success(function (data, status) {
								for (var clau in data) {
									if (data.hasOwnProperty(clau) && $scope.filtrarArtista(data[clau].code).length==0){ 
										$scope.artists.push(data[clau]);
									}
								}
								if(initCall) $scope.opcioSelectAutoComplete("",false,true);
							})
							.error(function (data, status) {
								alert("Error obtaining artist data in Auto complete");
							});
					}
					else{
						$http.post(rutaIn+'apiRest.php/rest/LP/filter',parametres)
							.success(function (data, status) {
								for (var clau in data) {
									if (data.hasOwnProperty(clau) && $scope.filtrarLPs(data[clau].code).length==0){ 
										$scope.LPs.push(data[clau]);
									}
								}
							})
							.error(function (data, status) {
								alert("Error obtaining LP data in Auto complete");
							});
					}
				}
				else{
					$scope.nomEntratAuC="";
				}
			}, 300); // esperem 500ms
	}
	
	//Pre: 	- - 
	//Post:	S'han obtingut del servidor totes les LP de "artist" 
	$scope.getArtistLPS=function(artist){
		$http.get(rutaIn+'apiRest.php/rest/artist/'+artist.code+'/LP')
			.success(function (data, status) {
				for (var clau in data) {
					if (data.hasOwnProperty(clau) && $scope.filtrarLPs(data[clau].code).length==0){
						$scope.LPs.push(data[clau]);
					}
				}
			})
			.error(function (data, status) {
				alert("Error obtaining artist LP");
				console.log(data);
			});
	}
	
	
	//Pre: 	- - 
	//Post:	S'han modificat les dades de l'artista "artist"
	$scope.setArtist=function(artist){
		var parametres = {
						name:  $scope.nameArtAct,
						description: $scope.descArtAct
					};	
		$http.put(rutaIn+'apiRest.php/rest/artist/'+artist.code,parametres)
			.success(function (data, status) {
				for (var clau in data) {
					if (data.hasOwnProperty(clau)){
						$scope.artists = $scope.artists.filter(function(artistAct){
							return artistAct.code!==data[clau].code;
						});	
						$scope.artists.unshift(data[clau]);
					}
				}
			})
			.error(function (data, status) {
				alert("Error updating artist");
				console.log(data);
			});
	}
	
	//Pre: 	- - 
	//Post:	S'han modificat les dades de l'LP "lp"
	$scope.setLP=function(lp){
		var parametres = {
						name:  $scope.nameLPAct,
						description: $scope.descLPAct,
						artistId: lp.fk_artist
					};	
		$http.put(rutaIn+'apiRest.php/rest/LP/'+lp.code,parametres)
			.success(function (data, status) {
				for (var clau in data) {
					if (data.hasOwnProperty(clau)){
						$scope.LPs = $scope.LPs.filter(function(lpAct){
							return lpAct.code!==data[clau].code;
						});	
						$scope.LPs.unshift(data[clau]);
					}
				}
			})
			.error(function (data, status) {
				alert("Error updating LP");
				console.log(data);
			});
	}
	
	//Pre: 	- - 
	//Post:	S'ha eliminat l'artista "artist" i tots els seus "lp"
	$scope.deleteArtist=function(artist){
		$http.delete(rutaIn+'apiRest.php/rest/artist/'+artist.code)
			.success(function (data, status) {
				$scope.artists = $scope.artists.filter(function(artAct){
					return artAct.code!==artist.code;
				});	
				$scope.LPs = $scope.LPs.filter(function(lpAct){
					return lpAct.fk_artist!==artist.code;
				});
				$scope.elSel.artistSelected="";
				$scope.contVar.artistLPList=false;
			})
			.error(function (data, status) {
				alert("Error deleting artist");
				console.log(data);
			});
	}
	
	//Pre: 	- - 
	//Post:	S'ha eliminat l'LP "lp"
	$scope.deleteLP=function(lp){
		$http.delete(rutaIn+'apiRest.php/rest/LP/'+lp.code)
			.success(function (data, status) {
				$scope.LPs = $scope.LPs.filter(function(lpAct){
					return lpAct.code!==lp.code;
				});	
				$scope.elSel.LPSelected="";
			})
			.error(function (data, status) {
				alert("Error deleting LP");
				console.log(data);
			});
	}

	//FUNCIONS FILTRARTGE - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	
	//Pre: 	- - 
	//Post:	S'han obtingut el numero de LP's d'un "artist" 
	$scope.getNumberArtistLPS=function(artist){
		var found = $filter('filter')($scope.LPs, {fk_artist: artist.code}, true);
		return found.length;
	}
	
	//Pre: 	- - 
	//Post:	S'han retornat l'artista amb codi "id" de la llista d'artistes
	$scope.filtrarArtista = function(id) {
		var found = $filter('filter')($scope.artists, {code: id}, true);
		return found;
	}
	
	//Pre: 	- - 
	//Post:	S'han retornat l'LP amb codi "id" de la llista de LPs
	$scope.filtrarLPs = function(id) {
		var found = $filter('filter')($scope.LPs, {code: id}, true);
		return found;
	}
	

	//OBTENCIO D'ALGUNES DADES DELA BD (SET INICIAL)
	$scope.opcioSelectAutoComplete("",true,true);
});