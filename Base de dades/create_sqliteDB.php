<?php
	//Creació i inicialitzacio de la BD
	
	//Establim connexio amb la BD
	require 'initDB.php';
	$db = new dataBase("./");
	if(!$db) {
			echo $db->lastErrorMsg();
	} 
	else {
		//Creacio estructura de la BD ----------------------------------------------------------
		echo "Opened database successfully\n";
		$db->exec('DROP TABLE IF EXISTS artist;');
		$db->exec('DROP TABLE IF EXISTS LP;');
		$db->exec('CREATE TABLE IF NOT EXISTS artist 
			(	code INT PRIMARY KEY NOT NULL,
				name STRING NOT NULL,
				description TEXT 
			);');
		$db->exec('CREATE TABLE IF NOT EXISTS LP 
			(	code INT PRIMARY KEY NOT NULL,
				name STRING NOT NULL,
				description TEXT,
				fk_artist INT,
				CONSTRAINT FK_artist FOREIGN KEY (fk_artist) REFERENCES artist(CODE)					
			);');
			
		//Inicialitzcio de la taula d'artistes,se'n afegeixen 5 per defecte -------------------
		$names = array("Duncan Dhu", "Nacha pop", "Celtas cortos", "Mago de oz", "Mecano"); 
		$descriptions = array(
			trim(preg_replace('/\s\s+/', ' ', "Duncan Dhu is a Spanish group created in San Sebastián, Spain in 1984. Its original members were Mikel Erentxun 
			(former singer in \"Aristogatos\"), Diego Vasallo and Juan Ramón Viles (former members of \"Los Dalton\"). Nowadays, it mainly consists of Mikel and Diego.")),
			trim(preg_replace('/\s\s+/', ' ', "Nacha Pop was a Spanish pop/rock group active from 1978 to 1988. The band was formed by Antonio Vega 
			and Nacho García Vega (guitars and vocals), Carlos Brooking (bass) and Ñete (drums). They signed with 
			Spanish EMI subsidiary Hispavox in 1980, and their debut album was produced by Teddy Bautista. Several 
			albums followed, including 1987's El Momento, which was produced by Carlos Narea. Their final concerts 
			took place on October 19 and 20, 1988, after which the group disbanded.")),
			trim(preg_replace('/\s\s+/', ' ', "Celtas Cortos is a Spanish music group of celtic rock who by 2006 had sold over two million records, cassettes and cds.
			It was formed in Valladolid (Castilla y León) in 1986. Eight friends, four of which played in the group Almenara 
			decided to participate in a music contest under the name \"Colectivo Eurofolk\". They won the first prize and continued 
			to play together changing their name to Celtas Cortos.Nacho Castro, the former drummer, suggested the name based on 
			his favourite tobacco.")),
			trim(preg_replace('/\s\s+/', ' ', "Mägo de Oz (Spanish for Wizard of Oz, with a metal umlaut) are a Spanish folk metal band from Begoña, Madrid formed in 
			mid-1988 by drummer Txus di Fellatio. The band became well known for the strong Celtic feel to their music strengthened 
			through their consistent usage of a violinist and flautist. The name for the band was chosen, according to founding member Txus, 
			because \"life is a yellow brick road, on which we walk in the company of others searching for our dreams.\"")),
			trim(preg_replace('/\s\s+/', ' ', "Mecano was a Spanish pop band formed in 1981 and active until 1992. Mecano became one of the most successful Spanish pop 
			bands of all time. The band is still the best-selling Spanish band to this date, reaching over 25 million albums worldwide.
			They were considered by some to be avant-garde for their time and part of la Movida Madrileña countercultural movement. They 
			had a brief comeback in 1998")));
				
		for ($i = 0; $i < sizeof($names); $i++) {
				echo "New artist ".$names[$i]." has been added \n";
				$db->exec('INSERT INTO artist VALUES 
					('.rand(1,1000).',"'.$names[$i].'","'.str_replace("\"","\"\"",$descriptions[$i]).'");');
		}
		
		$db->close();	
	}
?>